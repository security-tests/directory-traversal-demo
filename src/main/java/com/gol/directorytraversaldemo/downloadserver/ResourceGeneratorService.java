package com.gol.directorytraversaldemo.downloadserver;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.PostConstruct;

import static com.gol.directorytraversaldemo.downloadserver.Config.DS_DIR;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.Files.createDirectory;
import static java.nio.file.Files.exists;
import static java.nio.file.Files.write;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;

@Service
public class ResourceGeneratorService {

    @PostConstruct
    private void init() throws IOException {
        if (!exists(Paths.get(DS_DIR))) {
            createDirectory(Paths.get(DS_DIR));
        }
        Path path = Paths.get(DS_DIR, "file1");
        Files.deleteIfExists(path);
        write(path, "Sample file served by DL :)".getBytes(UTF_8), CREATE);
    }
}
