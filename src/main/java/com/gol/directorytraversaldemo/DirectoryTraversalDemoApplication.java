package com.gol.directorytraversaldemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class DirectoryTraversalDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DirectoryTraversalDemoApplication.class, args);
    }

}
